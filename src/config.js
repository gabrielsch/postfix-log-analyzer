export const API_BASE_URI = process.env.isProduction ? 
    `${window.location.origin}/api` :
    'http://localhost:5000/api'