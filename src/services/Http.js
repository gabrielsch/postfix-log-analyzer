import axios from 'axios'
import { API_BASE_URI } from '../config'
import { Auth } from './Auth'

const Client = axios.create({
    baseURL: API_BASE_URI,
});

Client.interceptors.request.use(function(config) {
    const token = Auth.getToken()

    if (token) {
        config.headers.Authentication = `${token}`
    }
    
    return config
})


export { Client };