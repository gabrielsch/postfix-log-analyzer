import { Client } from './Http'
import decode from 'jwt-decode'

const AUTH_URI = 'https://node-ts-api-authentication.herokuapp.com/auth';

export const Auth = {
    async login(username, password) {   
        try {
            const { data } = await Client.post(AUTH_URI, {
                user: username,
                password: password
            })
            if (!data.hasOwnProperty('token')) {
                return false
            }
            window.localStorage.setItem('token', data.token)            
            return true
        } catch (error) {
            return false
        }
        
    },
    async isLogged() {
        return this.getToken() != null;
    },
    getToken() {
        const token = window.localStorage.getItem('token');
        if (token == null) return null;

        const decoded = decode(token);
        const now = Math.floor(Date.now() / 1000);
        if (decoded.exp < now) {
            window.localStorage.removeItem('token');
            return null;
        }

        return window.localStorage.getItem('token')
    }
}